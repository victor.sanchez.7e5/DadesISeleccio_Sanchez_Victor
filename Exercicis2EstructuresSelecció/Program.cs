﻿using Exercicis2EstructuresSelecció;

internal class Program
{

    public void Menu()
    {
        string opcio;
        do
        {

            Console.WriteLine("1- ExxamGrade");
            Console.WriteLine("2- IsLeapYear");
            Console.WriteLine("3- WillWeFightForThecookiess");
            Console.WriteLine("0 - Sortir");
            opcio = Console.ReadLine();

            switch (opcio)
            {
                case "1":
                    ExamGrade ex = new ExamGrade();
                    ex.ExxamGrade();
                    break;
                case "2":
                    IsLeapYear Lp = new IsLeapYear();
                    Lp.IsLeapYYear();
                    break;
                case "3":
                    NextSecond ns = new NextSecond();
                    ns.NeXXtSecond();
                    break;
                case "0":
                    Console.WriteLine("Press Enter to end the program"); break;
                default: Console.WriteLine("Opcio Incorrecta"); break;


            }
        } while (opcio != "0");

    }
    private static void Main(string[] args)
    {
        var menu = new Program();
        menu.Menu(); 
                

    }
}
