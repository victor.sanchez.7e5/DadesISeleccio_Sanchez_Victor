﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 04-10 2022
Descripcio: - L'usuari introdueix una hora amb tres enters (hores, minuts i segons).

            - Imprimeix l'hora que serà al cap d'un segon.

*******************************************************************************/

namespace Exercicis2EstructuresSelecció
{
    internal class NextSecond
    {
        public void NeXXtSecond()
        {
            Console.Write("Introdueix la hora :");

            int h = Convert.ToInt32(Console.ReadLine());

            Console.Write("Introdueix els minuts :");

            int m = Convert.ToInt32(Console.ReadLine());

            Console.Write("Introdueix els segons :");

            int s = Convert.ToInt32(Console.ReadLine());

            if (h < 0|| m < 0||s < 0)
            {
                Console.WriteLine("Hora incorrecta !");
                return; 
            }

            if (s == 59 && m!=59 )
            {
                s = 00;
                m++;
                Console.WriteLine($" La hora actual és : {h} h: {m} m: {s} s ");
            }

            else if (m == 59 && s ==59 && h!=23)
            {
                m = 00;
                s = 00;
                h++;
                Console.WriteLine($" La hora actual és : {h} h: {m} m: {s} s ");

            }
            else if (h == 23 && m ==59 && s == 59 )
            {
                h = 00;
                m = 00;
                s = 00; 
                Console.WriteLine ($" La hora actual és : {h} h: {m} m: {s} s ");
            }else
            {
                s++;
                Console.WriteLine($" La hora actual és : {h} h: {m} m: {s} s ");
            }

        }
    }
}
