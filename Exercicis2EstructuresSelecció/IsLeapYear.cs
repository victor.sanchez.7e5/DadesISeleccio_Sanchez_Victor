﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 4-10 2022
Descripcio: L'usuari introdueix un any. Indica si és de traspàs printant "2020 és any de traspàs" o "2021 no és any de traspàs".

*******************************************************************************/



namespace Exercicis2EstructuresSelecció
{
    public class IsLeapYear
    {
        public void IsLeapYYear()
        {
            Console.Write("Introdueix un any:");

            int any = Convert.ToInt32(Console.ReadLine());

            if ((any % 4 == 0) && (any % 100 != 0))
            {

                Console.WriteLine("{0} es de traspas", any);

            }
            else if ((any % 100 == 0) && (any % 400 == 0))
            {

                Console.WriteLine("{0} es de traspas", any);
            }
            else
            {
                Console.WriteLine("{0} no es de traspas", any);
            }

        }
    }
}

