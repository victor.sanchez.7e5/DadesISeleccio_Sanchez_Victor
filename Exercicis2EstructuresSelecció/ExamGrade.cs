﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 4-10-2022
Descripcio: 	- L'usuari escriu un valor que representa una nota

                - Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons el la nota numèrica introduïda
*******************************************************************************/

namespace Exercicis2EstructuresSelecció
{
    public class ExamGrade
    {
        public void ExxamGrade()
        {
            Console.Write("Introdueix la teva nota:");

            double nota = Convert.ToDouble(Console.ReadLine());

            if (10 >= nota && nota >= 9)
            {

                Console.WriteLine("Excelent");

            }
            else if (9 > nota && nota >= 7)
            {

                Console.WriteLine("Notable");

            }
            else if (7 > nota && nota >= 6)
            {

                Console.WriteLine("Be");

            }
            else if (6 > nota && nota >= 5)
            {

                Console.WriteLine("Suficient");

            }
            else if (5 > nota && nota >= 0)
            {

                Console.WriteLine("Suspes");

            }
            else
            {

                Console.WriteLine("Nota invalida");
            }
        }
    }
}
