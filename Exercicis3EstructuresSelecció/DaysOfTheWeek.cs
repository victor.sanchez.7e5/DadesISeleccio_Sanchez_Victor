﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicis3EstructuresSelecció
{
    public class DaysOfTheWeek
    {

        public void DaysOfTheWeekk()
        {
            //Console.WriteLine(" Escriu un número del 1 al 7 :");

            int dia = 0 ;

            while (dia < 1 || dia > 7)
            {
                Console.WriteLine(" Escriu un número del 1 al 7 :");

                dia = Convert.ToInt32(Console.ReadLine());

            }

            switch (dia)
            {
                case 1:
                    Console.WriteLine("Dillins");
                    break; 
                case 2:
                    Console.WriteLine("Dimarts");
                    break;
                case 3:
                    Console.WriteLine("Dimecres");
                    break;
                case 4:
                    Console.WriteLine("Dijous"); 
                    break;
                case 5:
                    Console.WriteLine("Divendres");
                    break;
                case 6:
                    Console.WriteLine("Disabte");
                    break;
                case 7:
                    Console.WriteLine("Diumenge");
                    break; 
            }


        }
    }
}
