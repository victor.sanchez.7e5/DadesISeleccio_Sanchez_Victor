﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 04-10 2022
Descripcio: L'usuari escriu un valor que representa una nota

            Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons  la nota numèrica introduïda

*******************************************************************************/

namespace Exercicis3EstructuresSelecció
{
    internal class ExamGradeSwitch
    {
        public void EXXamSwitch()
        {
            Console.Write("Introdueix la teva nota:");

            int nota = Convert.ToInt32(Console.ReadLine());

            

            switch (nota)
            {
                case 10:
                    Console.WriteLine("Excelent"); 
                    break;
                case 9:
                    Console.WriteLine("Excelent");
                    break ;
                case 8:
                    Console.WriteLine("Notable");
                    break;
                case 7:
                    Console.WriteLine("Notable");
                    break;
                case 6:
                    Console.WriteLine("Bé");
                    break;
                case 5:
                    Console.WriteLine("Suficient");
                    break;
                case < 5:
                    Console.WriteLine("Suspes");
                    break;
                default:
                    Console.WriteLine("Nota Incorrecta"); break;
            }


        }
        
    }
}
