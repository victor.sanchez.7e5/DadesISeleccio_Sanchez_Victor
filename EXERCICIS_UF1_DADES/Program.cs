﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 20-09 2022
Descripcio: - Exercicis document del Classroom
*******************************************************************************/

internal class Program
{
    private static void Main(string[] args)
    {
        /* EXEMPLE CODI ASCII   
        char a = 'A';
        int aA = Convert.ToInt32(a);
        Console.Write(aA);
        */

        //E1();
        //E2();
        //E3();
        //E4();
        //E5(); 
        //E6();

        //que imprimeixi els valors parells fins al 20
        for (int i = 0; i <= 20; i++)
        {

            if (i % 2 == 0) Console.WriteLine(i);
       
        }
        Console.WriteLine();
        //
        for (int par = 0,senar =1; par <= 20; par += 2, senar+=2)
        {
            Console.WriteLine(par);
            Console.WriteLine(senar);
        }
    }

    private static void E1()
    {
        Console.WriteLine("Hello World");
    }

    private static void E2()
    {
        Console.WriteLine("Escriu un enter  ");
        int enter = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine(2 * enter);

    }
    private static void E3()
    {
        Console.WriteLine("Escriu un enter  ");
        int enter = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Escriu un altre enter  ");
        int enter2 = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine(enter + enter2);
    }

    private static void E4()
    {
        int amp;
        int ll;

        Console.WriteLine("Escriu dos nombres enters que representen amplada i llargada.");
        Console.WriteLine("Amplada:  ");
        amp = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Llargada:  ");
        ll = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine($"Area= {amp * ll}");

    }

    private static void E5()
    {


        Console.WriteLine("Escriu dos nombres enters que representen amplada i llargada.");
        Console.Write("a =  ");
        int a = Convert.ToInt32(Console.ReadLine());
        Console.Write("b =  ");
        int b = Convert.ToInt32(Console.ReadLine());
        Console.Write("c =  ");
        int c = Convert.ToInt32(Console.ReadLine());
        Console.Write("d =  ");
        int d = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine($"El resultat de la operació = {(a + b) * (c % d)}");

    }

    private static void E6()
    {
        /*En una escola tenim tres classes i volem saber quin és el nombre de taules que necessitarem
tenir en total. Dependrà del nombre d'alumnes per aula. Cal tenir en compte que a cada taula hi
caben 2 alumnes.*/
        Console.Write("Alumnes aula 1 =  ");
        int a1 = Convert.ToInt32(Console.ReadLine());
        Console.Write("Alumnes aula 2 =  ");
        int a2 = Convert.ToInt32(Console.ReadLine());
        Console.Write("Alumnes aula 3 =  ");
        int a3 = Convert.ToInt32(Console.ReadLine());

        a1 = (a1 / 2) + (a1 % 2);
        a2 = (a2 / 2) + (a2 % 2);
        a3 = (a3 / 2) + (a3 % 2);

        Console.WriteLine($" El número total de pupitres és = {a1 + a2 + a3}");

        /* Amb if i tres variables més :  
        int x1 = a1;
        int x2 = a2;
        int x3 = a3;

        a1 /= 2;
        a2 /= 2;
        a3 /= 2;

        if (x1 % 2 != 0) a1 = a1 + 1;
        if (x2 % 2 != 0) a2 = a2 + 1;
        if (x3 % 2 != 0) a3 = a3 + 1;
        Console.WriteLine($" El número total de pupitres és = {a1 + a2 + a3}");    */

    }

    private static void E7()
    {

    }
}