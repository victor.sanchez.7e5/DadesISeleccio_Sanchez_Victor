﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 28-09 2022
Descripcio: 	- Volem comparar quina pizza és més gran, entre una rectangular i una rodona

	            - L'usuai entra el diametre d'una pizza rodona

	            - L'usuari entra els dos costats de la pizza rectangular

	            - Imprimeix "Compra la rodona" si la pizza rodona és més gran, o "Compea la rectangular" en qualsevol altre cas.
*******************************************************************************/


namespace Exercicis1EstructuresSelecció
{
    public class WhichPizzaShouldIBuy
    {
        public void WhichPizza()
        {
            Console.Write("Introdueix el diametre de la pizza:");

            double diametre = Convert.ToDouble(Console.ReadLine());

            Console.Write("Introdueix el costat a de la pizza:");

            double costat = Convert.ToDouble(Console.ReadLine());

            Console.Write("Introdueix el costat b de la pizza:");

            double altura = Convert.ToDouble(Console.ReadLine());

            double areaCircular = 3.1415 * (diametre * diametre) / 4;

            double areaRectangle = costat * altura;

            if (areaRectangle > areaCircular)

            {

                Console.WriteLine("Comprar pizza rectangular");

            }
            else
            {

                Console.WriteLine("Comprar pizza rodona");


            }
        }
    }
}
