﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 28-09 2022
Descripcio: 	- Printa el valor absolut d'un enter entrat per l'usuari.
*******************************************************************************/

namespace Exercicis1EstructuresSelecció
{
    internal class AbsoluteNumber
    {
        public void AbsolutNumber()
        {
            Console.Write("Introdueix un nombre enter:");

            int num1 = Convert.ToInt32(Console.ReadLine());
            
            if (num1 < 0) 
            { 
                num1 = -num1;
            }

            Console.WriteLine(" El valor absolut es " + num1);

        }
        
    }
}
