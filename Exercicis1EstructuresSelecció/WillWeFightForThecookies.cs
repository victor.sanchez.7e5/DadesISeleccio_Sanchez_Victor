﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 28-09 2022
Descripcio: - Introdueix el número de persones i el número de galetes.

	        - Si a tothom li toquen el mateix número de galetes imprimeix "Let's Eat!", sinó imprimeix "Let's Fight"
*******************************************************************************/

namespace Exercicis1EstructuresSelecció
{
    public  class WillWeFightForThecookies
    {
        public void WillWeFightForThecookiess ()
        {
            Console.Write("Introdueix el número de persones :");

            int persones = Convert.ToInt32(Console.ReadLine());

            Console.Write("Introdueix el número de galetes :");

            int galetes = Convert.ToInt32(Console.ReadLine());

            if (galetes % persones == 0 )
            {
                Console.WriteLine($" Let's eat !"); 
            }
            else
            {
                Console.WriteLine($" Let's fight !");
            }

        }
    }
}

