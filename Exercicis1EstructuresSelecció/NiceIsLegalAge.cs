﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 28-09 2022
Descripcio: - Demana l'edat de l'usuari
	    - Printa  "Ets major d'edat", si és major d'edat.
*******************************************************************************/

namespace Exercicis1EstructuresSelecció
{ 
    public class NicelsLegalAge
    {
         
        public void NiceIsLegalAge()
        {

            Console.Write("Introdueix la teva edat:");

            int edat = Convert.ToInt32(Console.ReadLine());

            if (edat >= 18)
            {

                Console.WriteLine("Ets major d'edat");

            }
            else
            {

                Console.WriteLine("no ets major d'edat");
            }
        }
    }
}

