﻿/******************************************************************************
Author: Víctor Sánchez 
Date: 28-09 2022
Descripcio: 	- L'usuari escriu un enter

	            - Imprimeix  "bitllet vàlid"  si existeix un bitllet d'euros amb la quantitat entrada, "bitllet invàlid" en qualsevol altre cas.
*******************************************************************************/

namespace Exercicis1EstructuresSelecció
{
    public class NiceIsValidNote
    {
        public void NiceIIsValidNote()
        {
            Console.Write("Introdueix una quantitat de euros:");

            int billet = Convert.ToInt32(Console.ReadLine());

            if (billet == 5 || billet == 10 || billet == 20 || billet == 50 || billet == 100 || billet == 200 || billet == 500)
            {

                Console.WriteLine("Bitllet vàlid");

            }
            else
            {

                Console.WriteLine("Bitllet no vàlid");

            }
        }
    }
}
